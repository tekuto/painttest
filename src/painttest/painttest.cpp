﻿#include "sucrose/util/export.h"
#include "painttest/painttest.h"

#include "sucrose/basesystem/data.h"
#include "sucrose/basesystem/args.h"
#include "sucrose/basesystem/context.h"
#include "sucrose/window/painteventhandler.h"
#include "sucrose/window/paintevent.h"
#include "sucrose/window/eventhandlers.h"
#include "sucrose/gl/context.h"
#include "sucrose/gl/current.h"
#include "sucrose/thread/waittask.h"
#include "sucrose/thread/taskmanager.h"
#include "sucrose/common/unique.h"

#include <memory>
#include <new>
#include <utility>
#include <mutex>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    struct GameData
    {
        struct TaskData
        {
            std::mutex  mutex;
            bool        painting = false;
            bool        waitPainting = false;

            int     x;
            int     y;
            int     width;
            int     height;
        };

        sucrose::Unique< sucrose::GLContext >   glContextUnique;

        TaskData    taskData;

        sucrose::Unique< sucrose::WindowPaintEventHandler > paintEventHandlerUnique;

        sucrose::Unique< sucrose::ThreadTaskManager >   taskManagerUnique;

        sucrose::Unique< sucrose::WaitThreadTask >  drawTaskUnique;

        sucrose::Unique< sucrose::ThreadTaskManagerEnd >    taskManagerEndUnique;
    };

    void finalizeGameData(
        GameData &
    );

    struct FinalizeGameData
    {
        void operator()(
            GameData *  _gameData
        ) const
        {
            finalizeGameData( *_gameData );
        }
    };

    typedef std::unique_ptr<
        GameData
        , FinalizeGameData
    > GameDataUnique;

    void paintEvent(
        sucrose::WindowPaintEvent & _event
        , GameData &                _gameData
    )
    {
        auto &  taskData = _gameData.taskData;

        const auto  X = sucrose::getX( _event );
        const auto  Y = sucrose::getY( _event );
        const auto  WIDTH = sucrose::getWidth( _event );
        const auto  HEIGHT = sucrose::getHeight( _event );

#ifdef  DEBUG
        std::printf( "I:event x = %d\n", X );
        std::printf( "I:event y = %d\n", Y );
        std::printf( "I:event w = %d\n", WIDTH );
        std::printf( "I:event h = %d\n", HEIGHT );
#endif  // DEBUG

        {
            auto    lock = std::unique_lock< std::mutex >( taskData.mutex );

            taskData.x = X;
            taskData.y = Y;
            taskData.width = WIDTH;
            taskData.height = HEIGHT;
            taskData.waitPainting = true;

            if( taskData.painting ) {
                return;
            }
        }

        sucrose::execute(
            *( _gameData.taskManagerUnique )
            , *( _gameData.drawTaskUnique )
        );
    }

    void drawTask(
        sucrose::BaseContext &  _baseContext
        , GameData &            _gameData
    )
    {
        auto &  taskData = _gameData.taskData;

        auto    x = int( 0 );
        auto    y = int( 0 );
        auto    width = int( 0 );
        auto    height = int( 0 );

        {
            auto    lock = std::unique_lock< std::mutex >( taskData.mutex );

            taskData.painting = true;
            taskData.waitPainting = false;
            x = taskData.x;
            y = taskData.y;
            width = taskData.width;
            height = taskData.height;
        }

#ifdef  DEBUG
        std::printf( "I:draw x = %d\n", x );
        std::printf( "I:draw y = %d\n", y );
        std::printf( "I:draw w = %d\n", width );
        std::printf( "I:draw h = %d\n", height );
#endif  // DEBUG

        auto &  glContext = *( _gameData.glContextUnique );
        auto &  window = sucrose::getWindow( _baseContext );

        auto    glCurrentUnique = sucrose::unique(
            sucrose::newGLCurrent(
                glContext
                , window
            )
        );
        if( glCurrentUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:OpenGLカレントの生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  glCurrent = *glCurrentUnique;

        glClearColor( 0, 0, 0, 0 );
        glClear( GL_COLOR_BUFFER_BIT );

        glBegin( GL_TRIANGLES );

        glVertex2f( 0, 0 );
        glVertex2f( 1, 0 );
        glVertex2f( 1, 1 );

        glEnd();

        sucrose::swapBuffers( glCurrent );

        {
            auto    lock = std::unique_lock< std::mutex >( taskData.mutex );

            taskData.painting = false;

            auto &  waitPainting = taskData.waitPainting;
            if( waitPainting == false ) {
                return;
            }
        }

        sucrose::execute(
            *( _gameData.taskManagerUnique )
            , *( _gameData.drawTaskUnique )
        );
    }

    bool initTasks(
        sucrose::BaseContext &  _baseContext
        , GameData &            _gameData
    )
    {
        auto &  waitThreadPool = sucrose::getWaitThreadPool( _baseContext );

        auto    drawTaskUnique = sucrose::unique(
            sucrose::newWaitThreadTask(
                waitThreadPool
                , [
                    &_baseContext
                    , &_gameData
                ]
                {
                    drawTask(
                        _baseContext
                        , _gameData
                    );
                }
            )
        );
        if( drawTaskUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:描画タスクの生成に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    taskManager = static_cast< sucrose::ThreadTaskManager * >( nullptr );
        auto    taskManagerEnd = static_cast< sucrose::ThreadTaskManagerEnd * >( nullptr );
        if( sucrose::initializeThreadTaskManager(
            taskManager
            , taskManagerEnd
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:タスクマネージャの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto    taskManagerUnique = sucrose::unique( taskManager );
        auto    taskManagerEndUnique = sucrose::unique( taskManagerEnd );

        _gameData.taskManagerUnique = std::move( taskManagerUnique );

        _gameData.drawTaskUnique = std::move( drawTaskUnique );

        _gameData.taskManagerEndUnique = std::move( taskManagerEndUnique );

        return true;
    }

    GameData * initializeGameData(
        sucrose::BaseContext &  _baseContext
    )
    {
        auto    glContextUnique = sucrose::unique( sucrose::newGLContext() );
        if( glContextUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:OpenGLコンテキスト生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    gameDataUnique = std::unique_ptr< GameData >(
            new( std::nothrow )GameData{
                std::move( glContextUnique ),
            }
        );
        if( gameDataUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  gameData = *gameDataUnique;

        if( initTasks(
            _baseContext
            , gameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:各タスクの初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    paintEventHandlerUnique = sucrose::unique(
            sucrose::newWindowPaintEventHandler(
                [
                    &gameData
                ]
                (
                    sucrose::WindowPaintEvent & _event
                )
                {
                    paintEvent(
                        _event
                        , gameData
                    );
                }
            )
        );
        if( paintEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:描画イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        gameData.paintEventHandlerUnique = std::move( paintEventHandlerUnique );

        return gameDataUnique.release();
    }

    void finalizeGameData(
        GameData &  _gameData
    )
    {
        delete &_gameData;
    }

    bool initWindowEventHandlers(
        sucrose::BasesystemArgs &   _args
        , GameData &                _gameData
    )
    {
        auto    eventHandlersUnique = sucrose::unique( sucrose::newWindowEventHandlers() );
        if( eventHandlersUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  eventHandlers = *eventHandlersUnique;

        if( sucrose::add(
            eventHandlers
            , *( _gameData.paintEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( sucrose::setWindowEventHandlers(
            _args
            , eventHandlers
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合の設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

namespace painttest {
    bool initializePaintTest(
        sucrose::BasesystemData &   _basesystemData
    )
    {
        auto &  args = sucrose::getBasesystemArgs( _basesystemData );
        auto &  context = sucrose::getBaseContext( _basesystemData );

        auto    gameDataUnique = GameDataUnique(
            initializeGameData(
                context
            )
        );
        if( gameDataUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  gameData = *gameDataUnique;

        if( sucrose::setGameData(
            args
            , gameData
            , finalizeGameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ゲームデータの設定に失敗\n" );
#endif  // DEBUG

            return false;
        }
        gameDataUnique.release();

        if( initWindowEventHandlers(
            args
            , gameData
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ウィンドウイベントハンドラの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}
