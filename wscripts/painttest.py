# -*- coding: utf-8 -*-

from . import common

def getDependModules(
):
    return [
    ]

def getFeatures(
):
    return [
        'cxx',
        'cxxshlib',
    ]

def getTarget(
):
    return 'painttest'

def getSources(
):
    return {
        common.SOURCE_DIR : {
            'painttest' : [
                'painttest.cpp',
            ],
        },
    }

def getUse(
):
    return [
    ]

def getLib(
):
    return [
    ]

def getStlib(
    _context,
):
    return [
    ]
