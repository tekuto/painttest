# -*- coding: utf-8 -*-

from . import common

from waflib import Utils

import os.path

SUCROSE_HEADERS = 'sucrose-headers'
_SUCROSE_DIR = 'sucrose'

BUILD = 'build'
BUILD_DEBUG = 'debug'
BUILD_RELEASE = 'release'

COMPILER_TYPE = 'compilertype'
COMPILER_TYPE_GCC = 'gcc'
COMPILER_TYPE_MSVC = 'msvc'

LINKER_TYPE = 'linkertype'
LINKER_TYPE_LD = 'ld'
LINKER_TYPE_MSVC = 'msvc'

_OS_LINUX = 'linux'
_OS_WINDOWS = 'win32'

def _defaultValue(
    _VALUES,
):
    PLATFORM = Utils.unversioned_sys_platform()

    if PLATFORM in _VALUES:
        return _VALUES[ PLATFORM ]

    return None

OPTIONS = {
    SUCROSE_HEADERS : os.path.join(
        '..',
        _SUCROSE_DIR,
        common.INCLUDE_DIR,
    ),

    BUILD : BUILD_DEBUG,

    COMPILER_TYPE : _defaultValue(
        {
            _OS_LINUX : COMPILER_TYPE_GCC,
            _OS_WINDOWS : COMPILER_TYPE_MSVC,
        },
    ),
    LINKER_TYPE : _defaultValue(
        {
            _OS_LINUX : LINKER_TYPE_LD,
            _OS_WINDOWS : LINKER_TYPE_MSVC,
        },
    ),
}
