﻿#ifndef PAINTTEST_PAINTTEST_H
#define PAINTTEST_PAINTTEST_H

#include "sucrose/basesystem/data.h"
#include "sucrose/util/import.h"

namespace painttest {
    SUCROSE_FUNCTION_BOOL(
        initializePaintTest(
            sucrose::BasesystemData &
        )
    )
}

#endif  // PAINTTEST_PAINTTEST_H
